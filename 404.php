<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>404</title>
  <script src="https://cdn.tailwindcss.com"></script>
  <style>
    .go-to-home:hover {
      color: red;
    }
  </style>
</head>
<body class="bg-gray-100">
  <div class="flex justify-center items-center w-[100vw] h-[100vh]">
    <div class="flex flex-col text-center">
      <h2 class="text-center text-4xl mb-10 font-light">
        <div>요청하신 페이지를</div>
        <div class="text-red-500">찾을 수 없습니다.</div>
      </h3>
      <p class="font-thin mb-5">
        서비스 이용에 불편을 드려 죄송합니다.<br />페이지 이동을 원하시면 아래 버튼을 클릭해 주시기 바랍니다.
      </p>
      <a href="/" class="go-to-home" type="button">홈으로 이동</a>
    </div>
  </div>
</body>
</html>
