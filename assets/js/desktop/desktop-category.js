if (window.innerWidth >= 768) {
  // 카테고리 페이지 <오늘 핫한 뉴스>
  (($) => {
    new Swiper(".desktop-category-section-2 .swiper", {
      loop: true,
      spaceBetween: 20,
      pagination: {
        el: document.querySelector(".desktop-category-section-2 .swiper-pagination"),
        clickable: true,
      }
    });
  })(jQuery);

  // 카테고리 페이지 <인기 급상승> 뉴스
  (($) => {
    new Swiper(".desktop-category-section-3 .swiper", {
      loop: true,
      autoplay: true,
      spaceBetween: 20,

      // If we need pagination
      pagination: {
        el: document.querySelector(".desktop-category-section-3 .swiper-pagination"),
        clickable: true,
      },
    });
  })(jQuery);

}
