// 차트
(() => {
  const traffic_day_count_map = (Array.isArray(traffic_chart_30days_option) ? traffic_chart_30days_option : []).reduce((obj, item) => {
    obj[item.day] = parseInt(item.view_count, 10);
    return obj;
  }, {});

  const getLast30Days = () => {
    let dateArray = [];
    for(let i = 0; i < 30; i++) {
        let date = new Date();
        date.setDate(date.getDate() - i);
        dateArray.push(date.toISOString().slice(0,10));
    }
    return dateArray.reverse();
  }
  let last30Days = getLast30Days();

  const xaxis_categories = last30Days.map((day) => {
    return day.slice(5);
  });

  const series_data = last30Days.map((day) => {
    return traffic_day_count_map[day] || 0;
  });

  const maximum = Math.max(...series_data) * 1.2;

  const options = {
    series: [
    {
      name: "Visit",
      data: series_data,
    }
    ],
      chart: {
        height: 450,
        type: 'line',
        toolbar: {
          show: false
        },
        zoom: {
          enabled: false
        }
      },
    colors: ['#1e90ff'],
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth'
    },
    grid: {
      borderColor: 'blue',
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5
      },
    },
    markers: {
      size: 1
    },
    xaxis: {
      categories: xaxis_categories,
      title: {
        text: 'Day'
      }
    },
    yaxis: {
      title: {
        text: 'Visits'
      },
      min: 0,
      max: maximum,
    },
    legend: {
      position: 'top',
      horizontalAlign: 'right',
      floating: true,
      offsetY: -25,
      offsetX: -5
    }
  };

  const chart = new ApexCharts(
    document.querySelector("#apex_line1"),
    options
  );

  document.addEventListener('DOMContentLoaded', (event) => {
    chart.render();
  });
})(jQuery);

// date picker
(($) => {
  document.addEventListener('DOMContentLoaded', () => {
    /**
      * Korean translation for bootstrap-datepicker
      * This is a port from https: //github.com/moment/moment/blob/develop/src/locale/ko.js
    */
    Datepicker.locales.ko = {
      days: ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"],
      daysShort: ["일", "월", "화", "수", "목", "금", "토"],
      daysMin: ["일", "월", "화", "수", "목", "금", "토"],
      months: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
      monthsShort: ["1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월", "11월", "12월"],
      today: "오늘",
      clear: "삭제",
      format: "yyyy-mm-dd",
      titleFormat: "y년mm월",
      weekStart: 0
    };
    const elem = document.querySelector('.popular-post-list-container .date-range');
    const rangePicker = new DateRangePicker(elem, {
      'language': 'ko'
    });
  });
})(jQuery);