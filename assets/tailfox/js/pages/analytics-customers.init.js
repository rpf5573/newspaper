(() => {
  const traffic_day_count_map = (Array.isArray(traffic_chart_30days_option) ? traffic_chart_30days_option : []).reduce((obj, item) => {
    obj[item.day] = parseInt(item.view_count, 10);
    return obj;
  }, {});

  const getLast30Days = () => {
    let dateArray = [];
    for(let i = 0; i < 30; i++) {
        let date = new Date();
        date.setDate(date.getDate() - i);
        dateArray.push(date.toISOString().slice(0,10));
    }
    return dateArray.reverse();
  }
  let last30Days = getLast30Days();

  const xaxis_categories = last30Days.map((day) => {
    return day.slice(5);
  });

  const series_data = last30Days.map((day) => {
    return traffic_day_count_map[day] || 0;
  });

  const maximum = Math.max(...series_data) * 1.2;

  const options = {
    series: [
    {
      name: "Visit",
      data: series_data,
    }
  ],
    chart: {
    height: 450,
    type: 'line',
    toolbar: {
      show: false
    }
  },
  colors: ['#1e90ff'],
  dataLabels: {
    enabled: false,
  },
  stroke: {
    curve: 'smooth'
  },
  grid: {
    borderColor: 'blue',
    row: {
      colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
      opacity: 0.5
    },
  },
  markers: {
    size: 1
  },
  xaxis: {
    categories: xaxis_categories,
    title: {
      text: 'Day'
    }
  },
  yaxis: {
    title: {
      text: 'Visits'
    },
    min: 0,
    max: maximum,
  },
  legend: {
    position: 'top',
    horizontalAlign: 'right',
    floating: true,
    offsetY: -25,
    offsetX: -5
  }
  };

  var chart = new ApexCharts(document.querySelector("#chart"), options);
  chart.render();

  var chart = new ApexCharts(
    document.querySelector("#apex_line1"),
    options
  );

  document.addEventListener('DOMContentLoaded', (event) => {
    chart.render();
  });
})(jQuery);