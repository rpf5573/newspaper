<?php
get_header(); ?>

<main id="primary" class="site-main category-container">
  <!-- Desktop -->
  <div class="hidden md:block">
    <div class="wrap mt-5"> <?php
      np_template_desktop('category'); ?>
    </div>
  </div>

  <!-- Mobile -->
  <div class="block md:hidden"> <?php
    np_template_mobile('category'); ?>
  </div>
</main><!-- #main -->

<?php
get_footer();
