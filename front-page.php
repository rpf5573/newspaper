<?php
get_header(); ?>

<main id="primary" class="site-main">
  <!-- Desktop -->
  <div class="hidden md:block"> <?php
    np_template_desktop('front-page'); ?>
  </div>

  <!-- Mobile -->
  <div class="block md:hidden"> <?php
    np_template_mobile('front-page'); ?>
  </div>
</main><!-- #main -->

<?php
get_footer();
