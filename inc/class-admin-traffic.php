<?php

class NP_Admin_Traffic {
  public function __construct() {
    add_action( 'admin_menu', function() {
      add_menu_page( '트래픽', '트래픽', 'can_view_traffic', 'traffic', [$this, 'display_traffic_page'], '', 6 );
    } );
    add_action( 'admin_enqueue_scripts', function() {
      if (isset($_REQUEST['page']) && $_REQUEST['page'] === 'traffic') {
        $tailfox_uri = get_template_directory_uri() . '/assets/tailfox';
        wp_enqueue_style( 'tailfox-icons', $tailfox_uri . '/css/icons.min.css' );
        wp_enqueue_style( 'tailfox-tailwind', $tailfox_uri . '/css/tailwind.min.css' );
        wp_enqueue_style( 'tailfox-unicons', $tailfox_uri . '/libs/@iconscout/unicons/css/line.css' );
        wp_enqueue_style( 'tailfox-datepicker', $tailfox_uri . '/libs/vanillajs-datepicker/css/datepicker.min.css');

        // scripts
        wp_enqueue_script( 'tailfox-popperjs', $tailfox_uri . '/libs/@popperjs/core/umd/popper.min.js', '', '', true );
        wp_enqueue_script( 'tailfox-simplebar', $tailfox_uri . '/libs/simplebar/simplebar.min.js', '', '', true );
        wp_enqueue_script( 'tailfox-feather-icons', $tailfox_uri . '/libs/feather-icons/feather.min.js', '', '', true );
        wp_enqueue_script( 'tailfox-apexcharts', $tailfox_uri . '/libs/apexcharts/apexcharts.min.js', '', '', true );
        wp_enqueue_script( 'tailfox-datepicker', $tailfox_uri . '/libs/vanillajs-datepicker/js/datepicker-full.min.js', '', '', true );
        wp_enqueue_script( 'tailfox-memoent', $tailfox_uri . '/libs/moment/moment.js', '', '', true );

        wp_enqueue_script( 'tailfox-app', $tailfox_uri . '/js/app.js', '', '', true );

        $traffic = $this->get_traffic($this->get_selected_user_id_list(), $this->get_selected_category_id_list());
        
        wp_localize_script( 'tailfox-app', 'traffic_chart_30days_option', $traffic );
      }
    } );

    add_filter( 'traffic_author_id_list', [$this, 'filter_author_id_list'], 10, 1);
    add_filter( 'traffic_popluar_post_list_author_id_list', [$this, 'filter_author_id_list'], 10, 1);
    add_filter( 'traffic_total_visit_count_author_id_list', [$this, 'filter_author_id_list'], 10, 1);
    add_filter( 'traffic_author_id_list', [$this, 'filter_author_id_list_add_team_members'], 20, 1);
    add_filter( 'traffic_popluar_post_list_author_id_list', [$this, 'filter_author_id_list_add_team_members'], 20, 1);
    add_filter( 'traffic_total_visit_count_author_id_list', [$this, 'filter_author_id_list_add_team_members'], 20, 1);
  }

  public function get_traffic($author_id_list = [], $category_id_list = [], $period = 30) {
    global $wpdb;

    $author_id_list = apply_filters('traffic_author_id_list', $author_id_list);

    $params = [$period];

    $sql = " SELECT DATE(pv.date) AS day, COUNT(*) as view_count
      FROM {$wpdb->prefix}avc_page_visit_history as pv ";

    if (!empty($category_id_list)) {
      $sql .= " LEFT JOIN {$wpdb->term_relationships} AS tr ON pv.article_id = tr.object_id ";
    }
    
    $sql .= " LEFT JOIN {$wpdb->posts} AS p ON pv.article_id = p.ID ";

    $sql .= " WHERE DATE(pv.date) >= DATE_SUB(NOW(), INTERVAL %d DAY)
    AND pv.article_type = 'post' AND p.post_status = 'publish' ";

    if (!empty($category_id_list)) {
      $sql .= " AND tr.term_taxonomy_id IN (" . implode(',', $category_id_list) . ") ";
    }

    if (!empty($author_id_list)) {
      $sql .= " AND p.post_author IN (" . implode(',', $author_id_list) . ") ";
    }

    $sql .= "
      GROUP BY day
      ORDER BY day ASC;
    ";

    $sql = $wpdb->prepare($sql, $params);

    $results = $wpdb->get_results($sql);
    return $results;
  }

  public function get_total_visit_count($author_id_list = [], $category_id_list = [], $period = 'today') {
    global $wpdb;

    $author_id_list = apply_filters('traffic_total_visit_count_author_id_list', $author_id_list);

    // day, week, month, year
    $sql = "
        SELECT COUNT(*) 
        FROM `{$wpdb->prefix}avc_page_visit_history` as pv
    ";

    if (!empty($category_id_list)) {
      $sql .= " LEFT JOIN {$wpdb->term_relationships} AS tr ON pv.article_id = tr.object_id ";
    }

    // post_status를 조사해야 하기 때문에 무적권 JOIN을 한다
    $sql .= " LEFT JOIN {$wpdb->posts} AS p ON pv.article_id = p.ID ";

    if ($period === 'today') {
      $sql .= " WHERE DATE(pv.date) = CURDATE() ";
    } else if ($period === 'week') {
      $sql .= " WHERE WEEK(pv.date, 1) = WEEK(NOW(), 1) AND YEAR(pv.date) = YEAR(NOW()) ";
    } else if ($period === 'month') {
      $sql .= " WHERE MONTH(pv.date) = MONTH(CURDATE()) AND YEAR(pv.date) = YEAR(CURDATE()) ";
    } else if ($period === 'year') {
      $sql .= " WHERE YEAR(pv.date) = YEAR(CURDATE()) ";
    } else if (is_array($period) && count($period) === 2) {
      $start_date = $period[0];
      $end_date = $period[1];
      $sql .= " WHERE DATE(pv.date) >= '{$start_date}' AND DATE(pv.date) <= '{$end_date}' ";
    } else {
      $sql .= " WHERE DATE(pv.date) = CURDATE() ";
    }

    $sql .= " AND pv.article_type = 'post' AND p.post_status = 'publish' ";

    if (!empty($author_id_list)) {
      $sql .= " AND p.post_author IN (" . implode(',', $author_id_list) . ") ";
    }

    if (!empty($category_id_list)) {
      // 배열은 %s 로 넣으면 '(1, 2, 3)' 이렇게 들어가서 의도와 다르게 작동한다
      $sql .= " AND tr.term_taxonomy_id IN (" . implode(',', $category_id_list) . ") ";
    }
    
    $sql = $wpdb->prepare($sql, $params);


    return intval($wpdb->get_var($sql));
  }

  public function get_popluar_post_list($author_id_list = [], $category_id_list = [], $start_date, $end_date) {
    global $wpdb;

    $author_id_list = apply_filters('traffic_popluar_post_list_author_id_list', $author_id_list);

    $params = [$start_date, $end_date];

    $sql = " SELECT pv.article_id, COUNT(*) as view_count
      FROM {$wpdb->prefix}avc_page_visit_history as pv ";

    $sql .= " LEFT JOIN {$wpdb->posts} AS p ON pv.article_id = p.ID ";

    if (!empty($category_id_list)) {
      $sql .= " LEFT JOIN {$wpdb->term_relationships} AS tr ON pv.article_id = tr.object_id ";
    }

    $sql .= " WHERE DATE(pv.date) >= %s AND DATE(pv.date) <= %s
      AND pv.article_type = 'post' AND p.post_status = 'publish' ";

    if (!empty($category_id_list)) {
      // 배열은 %s 로 넣으면 '(1, 2, 3)' 이렇게 들어가서 의도와 다르게 작동한다
      $sql .= " AND tr.term_taxonomy_id IN (" . implode(',', $category_id_list) . ") ";
    }

    if (!empty($author_id_list)) {
      $sql .= " AND p.post_author IN (" . implode(',', $author_id_list) . ") ";
    }

    $sql .= "
      GROUP BY pv.article_id
      ORDER BY view_count DESC
      LIMIT 30;
    ";

    $sql = $wpdb->prepare($sql, $params);


    $results = $wpdb->get_results($sql);

    // Extracting article IDs
    $article_ids = array_map(function($item){
        return $item->article_id;
    }, $results);

    // Extracting visit counts
    $visit_counts = array_map(function($item){
        return $item->view_count;
    }, $results);

    $args = array(
      'post_type'      => 'post',
      'post_status'    => 'publish',
      'posts_per_page' => -1,
      'post__in'       => !empty($article_ids) ? $article_ids : [0], // 0을 넣으면 아무것도 안가져온다
    );

    $query = new WP_Query($args);

    $posts = array();
    if ($query->have_posts()) {
      $i = 0;
      while($query->have_posts()){
        $query->the_post();
        $post_id = get_the_ID();

        $categories = get_the_category();
        $category_names = array_map(function($category) {
          return $category->name;
        }, $categories);


        $posts[] = array(
          'title' => get_the_title(),
          'author' => get_the_author(),
          'date' => get_the_date(),
          'thumbnail_image_id' => get_post_thumbnail_id(),
          'post_link' => get_permalink(),
          'visit_count' => $visit_counts[$i],
          'edit_post_link' => get_edit_post_link($post_id),
          'categories' => $category_names
        );
        $i += 1;
      }

      wp_reset_postdata();
    }

    return $posts;
  }

  public function get_selected_user_id_list() {
    $selected_user = isset($_GET['traffic_user']) ? $_GET['traffic_user'] : '';
    if ($selected_user === 'all' || $selected_user === '') {
      // 닫혀있을때
      if (!np_get_traffic_open_to_all()) {
        // 현재 사용자가 팀장일때는 all 이나 ''는 팀장 id를 넣어준다
        $user_id = get_current_user_id();
        if (np_is_section_chief($user_id)) {
          return [$user_id];
        }
      }
      return [];
    }

    return [$selected_user];
  }

  public function get_selected_category_id_list() {
    $selected_category = isset($_GET['traffic_category']) ? $_GET['traffic_category'] : '';

    if ($selected_category === 'all' || $selected_category === '') {
      return [];
    }

    return [$selected_category];
  }

  public function filter_author_id_list($author_id_list) {
    $author_id_list = array_map(function($item) {
      return intval($item);
    }, $author_id_list);

    $user_id = get_current_user_id();
    if ($this->is_the_user_can_view_traffic($user_id)) {
      return $author_id_list;
    }

    // 내가 만약에 팀장라면, 그리고 해당 author가 모두 내 팀원이라면
    if (np_is_section_chief($user_id)) {
      $team_member_list = np_get_team_member_list($user_id);
      if (count($team_member_list) > 0) {
        $all_include = empty(array_diff($author_id_list, $team_member_list));
        if ($all_include) {
          return $author_id_list;
        }
      }
    }

    return [$user_id];
  }

  public function display_traffic_page() {
    ob_start();

    $selected_user = isset($_GET['traffic_user']) ? $_GET['traffic_user'] : null;
    $selected_category = isset($_GET['traffic_category']) ? $_GET['traffic_category'] : null;

    $selected_user_id_list = $this->get_selected_user_id_list();
    $selected_category_id_list = $this->get_selected_category_id_list();

    $categories = array_filter(get_categories(array('hide_empty' => 0)), function($category) {
      return $category->slug !== 'uncategorized';
    });
      
    $popular_post_list_start_date = $_GET['popular_post_list_start_date'] ?? date('Y-m-d', strtotime('-1 month'));
    $popular_post_list_end_date = $_GET['popular_post_list_end_date'] ?? date('Y-m-d');
    
    $popular_post_list = $this->get_popluar_post_list(
      $selected_user_id_list,
      $selected_category_id_list,
      $popular_post_list_start_date,
      $popular_post_list_end_date
    ); ?>

    <div class="px-4">
      <h1 class="text-2xl my-4">트래픽</h1>

      <!-- 필터 -->
      <div class="flex gap-2 mb-4">
        <form action="/wp-admin/admin.php" method="GET" autocomplete="off">
          <input type="hidden" name="page" value="traffic" /> <?php
            echo $this->author_filter_select($selected_user); ?>
          <select name="traffic_category" id="category-dropdown">
            <option value="" <?php echo $selected_category === null ? 'selected' : ''; ?> disabled>카테고리 선택</option>
            <option value="all" <?php echo $selected_category === 'all' ? 'selected' : ''; ?>>모든 카테고리</option>
            <?php
            foreach ($categories as $category) {
              $prefix = str_repeat('- ', $category->parent); ?>
              <option value="<?php echo $category->term_id; ?>" <?php echo $selected_category == $category->term_id ? 'selected' : ''; ?>><?php echo $prefix . $category->name; ?></option> <?php
            } ?>
          </select>
          <button type="submit" class="button-primary">이동</button>
        </form>
      </div>

      <!-- 30일간 조회수 그래프 -->
      <div class="mb-4">
          <div class="grid md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4">
              <div class="sm:col-span-12  md:col-span-12 lg:col-span-12 xl:col-span-12 ">
                  <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative h-full">
                      <div class="border-b border-dashed border-slate-200 dark:border-slate-700 py-3 px-4 dark:text-slate-300/70">
                          <div class="flex-none md:flex">
                              <h4 class="font-medium flex-1 self-center mb-2 md:mb-0">30일간 조회수</h4>
                          </div>
                      </div><!--end header-title-->
                      <div class="flex-auto p-4">
                          <div id="apex_line1" class="apex-charts"></div>
                      </div><!--end card-body-->
                  </div> <!--end card-->
              </div><!--end col-->                              
          </div><!--end inner-grid-->
      </div>

      <!-- 오늘, 이번주, 이번달, 올해 누적 조회수 -->
      <div class="mb-4">
          <div class="grid  grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-4">
              <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative ">                                
                  <div class="flex-auto p-4 text-center">
                      <h4 class="my-1 font-semibold text-2xl dark:text-slate-200"><?php echo $this->get_total_visit_count($selected_user_id_list, $selected_category_id_list, 'today'); ?></h4>
                      <h6 class="text-gray-400 mb-0 font-medium uppercase">오늘</h6>
                  </div><!--end card-body-->
              </div> <!--end card-->
              <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative ">                                
                  <div class="flex-auto p-4 text-center">
                      <h4 class="my-1 font-semibold text-2xl dark:text-slate-200"><?php echo $this->get_total_visit_count($selected_user_id_list, $selected_category_id_list, 'week'); ?></h4>
                      <h6 class="text-gray-400 mb-0 font-medium uppercase">이번주</h6>
                  </div><!--end card-body-->
              </div> <!--end card-->
              <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative ">                                
                  <div class="flex-auto p-4 text-center">
                      <h4 class="my-1 font-semibold text-2xl dark:text-slate-200"><?php echo $this->get_total_visit_count($selected_user_id_list, $selected_category_id_list, 'month'); ?></h4>
                      <h6 class="text-gray-400 mb-0 font-medium uppercase">이번달</h6>
                  </div><!--end card-body-->
              </div> <!--end card-->
              <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative ">                                
                  <div class="flex-auto p-4 text-center">
                      <h4 class="my-1 font-semibold text-2xl dark:text-slate-200"><?php echo $this->get_total_visit_count($selected_user_id_list, $selected_category_id_list, 'year'); ?></h4>
                      <h6 class="text-gray-400 mb-0 font-medium uppercase">올해</h6>
                  </div><!--end card-body-->
              </div> <!--end card-->
          </div>
      </div>

      <!-- 조회수 기준 내림차순 정렬 포스트 -->
      <div class="mb-4 popular-post-list-container">
          <div class="grid sm:grid-cols-12 md:grid-cols-12 lg:grid-cols-12 xl:grid-cols-12 gap-4 mb-4">                          
              <div class="sm:col-span-12  md:col-span-12 lg:col-span-12 xl:col-span-12 ">
                  <div class="bg-white dark:bg-slate-800 shadow  rounded-md w-full relative overflow-x-hidden">
                      <div class="border-b border-dashed border-slate-200 dark:border-slate-700 py-3 px-4 dark:text-slate-300/70">
                          <form action="/wp-admin/admin.php" method="GET" class="flex-none md:flex" autocomplete="off">
                              <h4 class="font-medium flex-1 self-center md:mb-0">인기 게시물</h4>
                              <div class="flex items-center gap-5">
                                <div class="flex gap-2 text-lg">
                                    <span>누적 조회수 :</span>
                                    <span><?php echo $this->get_total_visit_count($selected_user_id_list, $selected_category_id_list, [$popular_post_list_start_date, $popular_post_list_end_date]); ?></span>
                                  </div>
                                <div class="date-range flex">
                                    <input type="text" name="popular_post_list_start_date" value="<?php echo $_GET['popular_post_list_start_date']; ?>" id="start-date" class="form-input w-full rounded-none rounded-l-md  border border-slate-300/60 dark:border-slate-700 dark:text-slate-300 bg-transparent px-3 py-1 focus:outline-none focus:ring-0 placeholder:text-slate-400/70 placeholder:font-normal placeholder:text-sm hover:border-slate-400 focus:border-primary-500 dark:focus:border-primary-500  dark:hover:border-slate-700" placeholder="시작 날짜" aria-label="StartDate">
                                    <span class="inline-flex items-center px-3 text-sm text-slate-900 bg-slate-200  border border-r-0 border-l-0 border-slate-300 dark:bg-slate-600 dark:text-slate-400 dark:border-slate-600">~</span>
                                    <input type="text" name="popular_post_list_end_date" value="<?php echo $_GET['popular_post_list_end_date']; ?>" id="end-date" class="form-input w-full rounded-none rounded-r-md  border border-slate-300/60 dark:border-slate-700 dark:text-slate-300 bg-transparent px-3 py-1 focus:outline-none focus:ring-0 placeholder:text-slate-400/70 placeholder:font-normal placeholder:text-sm hover:border-slate-400 focus:border-primary-500 dark:focus:border-primary-500  dark:hover:border-slate-700" placeholder="종료 날짜" aria-label="EndDate">
                                    <input type="hidden" name="page" value="traffic" />
                                    <input type="hidden" name="traffic_user" value="<?php echo $_GET['traffic_user']; ?>" />
                                    <input type="hidden" name="traffic_category" value="<?php echo $_GET['traffic_category']; ?>" />
                                    <div class="flex ml-3">
                                      <button type="submit" class="px-3 py-1 text-base font-medium text-center text-white bg-primary-500 rounded hover:bg-primary-600  focus:outline-none dark:bg-primary-500 dark:hover:bg-primary-600 break-keep">조회</button>
                                    </div>
                                </div>
                              </div>
                          </form>                                    
                      </div><!--end header-title-->
                      <div class="grid grid-cols-1 p-4">
                          <div class="sm:-mx-6 lg:-mx-8">
                              <div class="relative overflow-x-auto block w-full sm:px-6 lg:px-8">
                                  <div class=" ">
                                      <table class="w-full">
                                          <thead class="bg-gray-50 dark:bg-gray-600/20">
                                              <tr>
                                                  <th scope="col" class="p-3 text-xs font-medium tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                      포스트 제목
                                                  </th>
                                                  <th scope="col" class="p-3 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400">
                                                      조회수
                                                  </th>
                                                  <th scope="col" class="p-3 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400">
                                                      카테고리
                                                  </th>
                                                  <th scope="col" class="p-3 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400">
                                                      글쓴이
                                                  </th>
                                                  <th scope="col" class="p-3 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400">
                                                      작성일자
                                                  </th>
                                                  <th scope="col" class="p-3 text-center text-xs font-medium tracking-wider text-gray-700 uppercase dark:text-gray-400">
                                                      수정
                                                  </th>
                                              </tr>
                                          </thead>
                                          <tbody> <?php
                                            if($popular_post_list){ ?>
                                                <?php
                                              foreach($popular_post_list as $popular_post){ ?>
                                                <tr class="bg-white border-b border-dashed dark:bg-gray-800 dark:border-gray-700">
                                                    <td class="p-3 text-sm font-medium whitespace-nowrap dark:text-white">
                                                        <div class="flex items-center">
                                                          <a href="<?php echo $popular_post['post_link']; ?>" target="_blank" class="block">
                                                            <img src="<?php echo np_get_image_url($popular_post['thumbnail_image_id']); ?>" alt="" class="mr-2 h-12 inline-block rounded">
                                                          </a>
                                                            <div class="self-center">
                                                                <h5 class="text-sm font-semibold text-slate-700 dark:text-gray-400">
                                                                  <a href="<?php echo $popular_post['post_link']; ?>" target="_blank"><?php echo $popular_post['title']; ?></a>
                                                                </h5>
                                                            </div>
                                                        </div>                                                    
                                                    </td>
                                                    <td class="view-count text-center p-3 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        <?php echo $popular_post['visit_count']; ?>
                                                    </td>
                                                    <td class="text-center p-3 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        <?php echo implode( ', ', $popular_post['categories'] ); ?>
                                                    </td>
                                                    <td class="text-center p-3 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        <?php echo $popular_post['author']; ?>
                                                    </td>
                                                    <td class="text-center p-3 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        <?php echo $popular_post['date']; ?>
                                                    </td>
                                                    <td class="text-center p-3 text-sm text-gray-500 whitespace-nowrap dark:text-gray-400">
                                                        <a href="<?php echo $popular_post['edit_post_link']; ?>" target="_blank"><i class="ti ti-edit text-xl text-gray-500 dark:text-gray-400"></i></a>
                                                    </td>
                                                </tr> <?php
                                              }
                                            } else {
                                              echo '<tr><td colspan="6" class="text-center p-5 text-base text-gray-500 whitespace-nowrap dark:text-gray-400">조회된 데이터가 없습니다.</td></tr>';
                                            } ?>
                                          </tbody>
                                      </table>
                                  </div>
                              </div>
                          </div>
                      </div><!--end card-body-->
                  </div> <!--end card-->
              </div><!--end col-->                                                          
          </div>
      </div>
    </div>
    <?php
    $output = ob_get_clean();
    echo $output;
  }

  public function author_filter_select($selected_user = '') {
    ob_start();

    $user_id = get_current_user_id();
    if (!($this->is_the_user_can_view_traffic($user_id))) {
      // 모두에게 오픈이 아니면 select를 안보여 주는게 맞긴 한데, 현재 사용자가 팀장인 경우에는 팀원들만 보여준다
      if (np_is_section_chief($user_id)) {
        $team_member_list = np_get_team_member_list($user_id);
        if (!empty($team_member_list)) { ?>
        <select name="traffic_user" id="user-list">
          <option value="" <?php echo $selected_user === null ? 'selected' : ''; ?> disabled>유저 선택</option>
          <option value="all" <?php echo $selected_user === 'all' ? 'selected' : ''; ?>>모든 사용자</option> <?php
          foreach ($team_member_list as $team_member_id) {
            $team_member = get_user_by('ID', $team_member_id);
            if (!$team_member) {
              continue;
            } ?>
            <option value="<?php echo $team_member_id; ?>" <?php echo $selected_user == $team_member_id ? 'selected' : ''; ?>><?php echo $team_member->display_name . ' (기자)'; ?></option> <?php
          } ?>
        </select> <?php
        }
        return ob_get_clean();
      }

      return '';
    } ?>

    <select name="traffic_user" id="user-list">
      <option value="" <?php echo $selected_user === null ? 'selected' : ''; ?> disabled>유저 선택</option>
      <option value="all" <?php echo $selected_user === 'all' ? 'selected' : ''; ?>>모든 사용자</option> <?php
      $users = get_users();
      $self = $this;

      // 팀장만 가져오고
      // get user ids of users who have role of section_chief
      $팀장_list = array_map(function($user) {
        $team_member_id_list = rwmb_get_value( 'team_member_list', [ 'object_type' => 'user' ], $user->ID );
        return array(
          'user_id' => $user->ID,
          'username' => $user->display_name,
          'role' => '팀장',
          'team_member_list' => array_filter(
              array_map(function($team_member_id) {
              $team_member = get_user_by('ID', $team_member_id);
              if (!$team_member) {
                return null;
              }
              return array(
                'user_id' => $team_member_id,
                'username' => $team_member->display_name,
                'role' => '기자',
              );
            }, $team_member_id_list), function($val) {
              return !is_null($val);
            })
        );
      }, get_users(['role' => 'section_chief']));

      foreach ($팀장_list as $팀장) { ?>
        <option value="<?php echo $팀장['user_id']; ?>" <?php echo $selected_user == $팀장['user_id'] ? 'selected' : ''; ?>><?php echo $팀장['username'] . ' (' . $팀장['role'] . ')'; ?></option> <?php
        if (!empty($팀장['team_member_list'])) {
          foreach ($팀장['team_member_list'] as $팀원) {
            if ($팀원) { ?>
              <option value="<?php echo $팀원['user_id']; ?>" <?php echo $selected_user == $팀원['user_id'] ? 'selected' : ''; ?>><?php echo '-- ' . $팀원['username'] . ' (' . $팀원['role'] . ')'; ?></option> <?php
            }
          }
        }
      } ?>
    </select> <?php

    return ob_get_clean();
  }

  public function is_the_user_can_view_traffic($user_id) {
    $isOpen = np_get_traffic_open_to_all();
    if ($isOpen) {
      return true;
    }

    $allowed_user_roles = ['administrator', 'chief_executive_officer', 'editor_in_chief', 'deputy_editor'];

    $user = get_userdata($user_id);
    $user_roles = $user->roles;

    if (count(array_intersect($user_roles, $allowed_user_roles)) > 0) {
      return true;
    }

    return false;
  }

  public function filter_author_id_list_add_team_members($author_id_list = []) {
    $team_member_id_list = [];

    // 팀장이 속해있는지 체크하고, 팀장이 속해있다면 팀원들도 추가해준다
    foreach($author_id_list as $author_id) {
      $user = get_user_by('ID', $author_id);
      if (np_is_section_chief($user->ID)) {
        $team_member_id_list = array_merge(np_get_team_member_list($user->ID), $team_member_id_list);
      }
    }

    return array_merge($author_id_list, $team_member_id_list);
  }
}
