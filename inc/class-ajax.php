<?php 

class NP_Ajax {
  public function __construct() {
    // localize script
    add_action( 'wp_ajax_nopriv_mobile_catgory_list_load_more', [$this, 'handle_mobile_catgory_list_load_more'] );
    add_action( 'wp_ajax_mobile_catgory_list_load_more', [$this, 'handle_mobile_catgory_list_load_more'] );

    add_action( 'wp_ajax_nopriv_desktop_section_4_load_more', [$this, 'handle_desktop_section_4_load_more'] );
    add_action( 'wp_ajax_desktop_section_4_load_more', [$this, 'handle_desktop_section_4_load_more'] );

    add_action( 'wp_ajax_nopriv_custom_comment', [$this, 'handle_custom_comment'] );
    add_action( 'wp_ajax_custom_comment', [$this, 'handle_custom_comment'] );

    add_action( 'wp_ajax_nopriv_custom_reply', [$this, 'handle_custom_reply'] );
    add_action( 'wp_ajax_custom_reply', [$this, 'handle_custom_reply'] );

    add_action( 'wp_ajax_nopriv_remove_comment', [$this, 'handle_remove_comment'] );
    add_action( 'wp_ajax_remove_comment', [$this, 'handle_remove_comment'] );

    add_action( 'wp_ajax_nopriv_remove_reply', [$this, 'handle_remove_reply'] );
    add_action( 'wp_ajax_remove_reply', [$this, 'handle_remove_reply'] );

    add_action( 'wp_ajax_nopriv_update_post_reaction', [$this, 'handle_update_post_reaction'] );
    add_action( 'wp_ajax_update_post_reaction', [$this, 'handle_update_post_reaction'] );
  }

  public function handle_mobile_catgory_list_load_more() {
    // @TODO np_get_latest_posts를 사용할 수 있다
    // 하지만 원래 루프도는 쪽과의 통일성을 지켜주는것도 괜찮을듯
    $cat_id = $_POST['category_id'];
    $page = $_POST['page'];

    $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'posts_per_page' => 10,
      'cat' => $cat_id,
      'paged' => $page
    );
    $query = new WP_Query($args);

    $output = '';

    if($query->have_posts()) {
      while($query->have_posts()) {
        $query->the_post();
        ob_start();
        np_template_mobile( 'category-section-1-list-item' );
        $output .= ob_get_clean();
      }
    }

    wp_reset_postdata();

    wp_send_json_success(array(
      'posts' => $output,
      'more'  => $query->max_num_pages > $page,
    ));
  }

  public function handle_desktop_section_4_load_more() {
    $page = $_POST['page'];

    $posts = np_get_latest_posts(16 + 1, $page); // 일부로 하나 더 가져와서, 마지막인지 아닌지 체크한다
    $count = count($posts);
    array_pop($posts);

    $output = '';
    foreach($posts as $post) {
      ob_start();
      np_template_desktop( 'front-section-4-list-item', ['post' => $post] );
      $output .= ob_get_clean();
    }

    wp_send_json_success(array(
      'posts' => $output,
      'more'  => $count > 16,
    ));
  }

  public function handle_custom_comment() {
    // Check if our nonce is set and verify it.
    if ( !isset( $_POST['comment_nonce'] ) || !wp_verify_nonce( $_POST['comment_nonce'], 'add_comment_nonce' ) ) {
      echo 'Nonce value is not valid.';
      die();
    }

    $username = sanitize_text_field($_POST['username']);
    $password = sanitize_text_field($_POST['password']);
    $content = sanitize_textarea_field($_POST['content']);
    $ip = $this->get_user_ip_address();
    $post_id = intval($_POST['post_id']);

    $commentdata = array(
      'comment_post_ID' => $post_id,
      'comment_content' => $content,
      'comment_approved' => 1,
    );

    $comment_id = wp_insert_comment($commentdata);

    if ($comment_id) {
      rwmb_set_meta($comment_id, 'comment_username', $username, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_password', $password, ['object_type' => 'comment']);
      $ip && rwmb_set_meta($comment_id, 'comment_ip', $ip, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_like', 0, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_dislike', 0, ['object_type' => 'comment']);
      wp_send_json_success('댓글이 생성되었습니다');
    } else {
      wp_send_json_error('댓글 생성중 에러가 발생했습니다');
    }
  }

  public function handle_custom_reply() {
    // Check if our nonce is set and verify it.
    if ( !isset( $_POST['reply_nonce'] ) || !wp_verify_nonce( $_POST['reply_nonce'], 'add_reply_nonce' ) ) {
      echo 'Nonce value is not valid.';
      die();
    }

    $username = sanitize_text_field($_POST['username']);
    $password = sanitize_text_field($_POST['password']);
    $content = sanitize_textarea_field($_POST['content']);
    $ip = $this->get_user_ip_address();
    $post_id = intval($_POST['post_id']);
    $parent_comment_id = intval($_POST['parent_comment_id']);

    $commentdata = array(
      'comment_post_ID' => $post_id,
      'comment_content' => $content,
      'comment_approved' => 1,
      'comment_parent' => $parent_comment_id,
    );

    $comment_id = wp_insert_comment($commentdata);

    if ($comment_id) {
      rwmb_set_meta($comment_id, 'comment_username', $username, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_password', $password, ['object_type' => 'comment']);
      $ip && rwmb_set_meta($comment_id, 'comment_ip', $ip, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_like', 0, ['object_type' => 'comment']);
      rwmb_set_meta($comment_id, 'comment_dislike', 0, ['object_type' => 'comment']);
      wp_send_json_success('댓글이 생성되었습니다');
    } else {
      wp_send_json_error('댓글 생성중 에러가 발생했습니다');
    }
  }

  public function get_user_ip_address() {
    $ip_address = '';
    if ( isset($_SERVER['HTTP_CLIENT_IP']) )
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    else if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if( isset($_SERVER['HTTP_X_FORWARDED']) )
        $ip_address = $_SERVER['HTTP_X_FORWARDED'];
    else if( isset($_SERVER['HTTP_FORWARDED_FOR']) )
        $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
    else if( isset($_SERVER['HTTP_FORWARDED']) )
        $ip_address = $_SERVER['HTTP_FORWARDED'];
    else if( isset($_SERVER['REMOTE_ADDR']) )
        $ip_address = $_SERVER['REMOTE_ADDR'];
    else
        $ip_address = 'UNKNOWN';

    return $ip_address;
  }

  public function handle_remove_comment() {
    $comment_id = intval($_POST['comment_id']);
    $password = sanitize_text_field($_POST['password']);
    $_password = rwmb_get_value('comment_password', ['object_type' => 'comment'], $comment_id);

    if ($password !== $_password) {
      wp_send_json_error('비밀번호가 일치하지 않습니다');
    }

    // Fetch all child comments of the comment to be deleted.
    $child_comments = get_comments(array('parent' => $comment_id));

    // Delete all child comments.
    foreach($child_comments as $comment) {
      wp_delete_comment($comment->comment_ID, true);
    }

    wp_delete_comment($comment_id, true);
    wp_send_json_success('댓글이 삭제되었습니다');
  }

  public function handle_remove_reply() {
    $reply_id = intval($_POST['reply_id']);
    $password = sanitize_text_field($_POST['password']);
    $_password = rwmb_get_value('comment_password', ['object_type' => 'comment'], $reply_id);

    if ($password !== $_password) {
      wp_send_json_error('비밀번호가 일치하지 않습니다');
    }
    wp_delete_comment($reply_id, true);
    wp_send_json_success('댓글이 삭제되었습니다');
  }

  public function handle_update_post_reaction() {
    // post_id 체크
    if ( !isset( $_POST['post_id'] ) || !is_numeric($_POST['post_id']) ) {
      wp_send_json_error( 'post_id가 없습니다' );
    }

    // post_reaction 체크
    if ( !isset( $_POST['reaction'] ) || !in_array($_POST['reaction'], ['like', 'impressed', 'suprised', 'sad', 'angry']) ) {
      wp_send_json_error( 'reaction이 없습니다' );
    }

    if ( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'update_post_reaction_nonce' ) ) {
      wp_send_json_error( '유효하지 않은 요청입니다' );
    }

    $field_id = 'post_reaction_' . $_POST['reaction'];
    $undo = isset($_POST['undo']) ? wp_validate_boolean($_POST['undo']) : false;
    $post_id = intval($_POST['post_id']);
    $count = np_get_post_reaction_count($post_id, $_POST['reaction']);

    $count = $undo ? $count - 1 : $count + 1;

    rwmb_set_meta( $post_id, $field_id, $count );
    wp_send_json_success('포스트 감정이 업데이트 되었습니다');
  }
}
