<?php

// Disable direct load
if (!defined('ABSPATH')) {
  die('-1');
}

final class NP_Posts_Filter
{
  private static $instance;
  private static $admin_notices = array();

  private function __construct()
  {
    add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'));
    add_action('admin_footer', array($this, 'admin_footer'));
  }

  public static function get_instance()
  {
    if (empty(self::$instance)) {
      self::$instance = new NP_Posts_Filter();
    }
    return self::$instance;
  }

  public function admin_enqueue_scripts()
  {
    wp_enqueue_script('np-posts-filter', get_template_directory_uri() . '/assets/js/posts-filter.js', array('jquery'), '1.0.0', true);
  }

  public function admin_footer()
  {
    $screen = get_current_screen();
  }
}
