<?php
$post_id = $args['post_id'];
$comment_id = $args['comment_id'];
$comment = np_get_comment_meta($comment_id);
$comment_args = array(
  'parent' => $comment_id,
  'status' => 'approve'
);
$reply_list = get_comments($comment_args);
?>

<li class="comment-list-item border-t-gray-200">
  <div class="flex m-3 md:m-5">
    <div class="h-10 w-10">
      <img class="rounded-full" src="https://cdn.salgoonews.com/image/newsroom/default-user.png" alt="">
    </div>
    <section class="ml-5 w-full">
      <header class="flex justify-between mb-3">
        <div class="flex items-center">
          <p class="text-sm md:text-base"><?php echo $comment['username']; ?></p>
          <span class="text-slate-400 text-xs mx-3">|</span>
          <span class="border rounded-sm px-1 text-xs mr-1 md:px-2 font-thin">ip</span>
          <span class="text-xs text-zinc-400 md:text-sm font-thin"><?php echo $comment['ip']; ?></span>
        </div>
        <button class="remove-comment-button text-xs text-slate-500 hover:underline" data-post-id="<?php echo $post_id; ?>" data-comment-id="<?php echo $comment_id; ?>">삭제</button>
      </header>
      <p class="text-slate-500 text-sm"><?php echo $comment['content']; ?></p>
      <footer class="flex mt-5">
        <button class="show-reply-form-button text-left text-xs text-slate-500 mr-5">답글 <span class="reply-count"><?php echo count($reply_list); ?></span></button>
        <div>
          <button class="comment-like-button text-xs mr-2"><i class="icon-thumbs-up-o"></i><span class="ml-1"><?php echo $comment['like_count']; ?></span></button>
          <button class="comment-dislike-button text-xs"><i class="icon-thumbs-down-o"></i><span class="ml-1"><?php echo $comment['dislike_count']; ?></span></button>
        </div>
      </footer>
    </section>
  </div>
  <ul class="reply-list hidden p-3 bg-gray-100 border-t md:p-4"> <?php
    foreach($reply_list as $reply) {
      $reply = np_get_comment_meta($reply->comment_ID);
      if (is_wp_error($reply)) {
        continue;
      } ?>
      <li class="reply-list-item mb-2 md:mb-8">
        <div class="flex md:mt-5">
          <p>
            <img src="https://www.salgoonews.com/image/comment/comment_type1_reply.png" alt="reply-img">
          </p>
          <div class="flex w-full ml-3 md:ml-5">
            <div class="h-10 w-10">
              <img class="rounded-full" src="https://cdn.salgoonews.com/image/newsroom/default-user.png" alt="">
            </div>
            <div class="ml-5 w-full">
              <header class="flex justify-between mb-3">
                <div class="flex items-center">
                  <p class="text-xs md:text-base"><?php echo $reply['username']; ?></p>
                  <span class="text-slate-400 text-xs mx-3">|</span>
                  <span class="border rounded-sm px-1 text-xs mr-1 md:px-2 font-thin">ip</span>
                  <span class="text-xs text-zinc-400 md:text-sm font-thin"><?php echo $reply['ip']; ?></span>
                </div>
                <button class="remove-reply-button text-xs text-slate-500 hover:underline" data-post-id="<?php echo $post_id; ?>" data-reply-id="<?php echo $reply['comment_id']; ?>">삭제</button>
              </header>
              <p class="text-slate-500 text-sm"> <?php
                echo $reply['content']; ?>
              </p>
              <footer class="flex mt-2 md:mt-5">
                <button class="comment-like-button text-xs mr-2"><i class="icon-thumbs-up-o"></i><span class="ml-1"><?php echo $reply['like_count']; ?></span></button>
                <button class="comment-dislike-button text-xs"><i class="icon-thumbs-down-o"></i><span class="ml-1"><?php echo $reply['dislike_count']; ?></span></button>
              </footer>
            </div>
          </div>
        </div>
      </li> <?php
    } ?>

    <li class="reply-list-item">
      <div class="flex mt-2 md:mt-5">
        <p>
          <img src="https://www.salgoonews.com/image/comment/comment_type1_reply.png" alt="reply-image">
        </p> 
        <div class="w-full ml-5"> <?php
          np_template_common('reply-form', array(
            'post_id' => $args['post_id'],
            'parent_comment_id' => $comment_id
          )); ?>
        </div>
      </div>
    </li>
  </ul>
</li>
