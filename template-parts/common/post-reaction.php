<?php
$post_id = $args['post_id'];
if (!$post_id) {
  return '';
}
?>

<div class="flex justify-center items-center text-center">
    <?php 
    // use nonce
    wp_nonce_field( 'update_post_reaction_nonce', 'post_reaction_nonce' ); ?>
    <button type="button" class="emoji-btns like" data-reaction="like" data-post_id="<?php echo $post_id; ?>">
      <span class="text-xs text-slate-500">
        좋아요
        <i class="number block not-italic text-black">
          <span class="reaction-count"> <?php
            echo np_get_post_reaction_count($post_id, 'like'); ?>
          </span>
        </i>
      </span>
      <div class="reaction-loading-indicator hidden"> <?php
        echo np_template_common('flash-loading-indicator'); ?>
      </div>
    </button>

    <button type="button" class="emoji-btns impressed" data-reaction="impressed" data-post_id="<?php echo $post_id; ?>">
      <span class="text-xs text-slate-500">
        감동이에요
        <i class="number block not-italic text-black">
          <span class="reaction-count"> <?php
            echo np_get_post_reaction_count($post_id, 'impressed'); ?>
          </span>
        </i>
      </span>
      <div class="reaction-loading-indicator hidden"> <?php
        echo np_template_common('flash-loading-indicator'); ?>
      </div>
    </button>

    <button type="button" class="emoji-btns suprised" data-reaction="suprised" data-post_id="<?php echo $post_id; ?>">
      <span class="text-xs text-slate-500">
        놀랐어요
        <i class="number block not-italic text-black">
          <span class="reaction-count"> <?php
            echo np_get_post_reaction_count($post_id, 'suprised'); ?>
          </span>
        </i>
      </span>
      <div class="reaction-loading-indicator hidden"> <?php
        echo np_template_common('flash-loading-indicator'); ?>
      </div>
    </button>

    <button type="button" class="emoji-btns sad" data-reaction="sad" data-post_id="<?php echo $post_id; ?>">
      <span class="text-xs text-slate-500">
        슬퍼요
        <i class="number block not-italic text-black">
          <span class="reaction-count"> <?php
            echo np_get_post_reaction_count($post_id, 'sad'); ?>
          </span>
        </i>
      </span>
      <div class="reaction-loading-indicator hidden"> <?php
        echo np_template_common('flash-loading-indicator'); ?>
      </div>
    </button>

    <button type="button" class="emoji-btns angry" data-reaction="angry" data-post_id="<?php echo $post_id; ?>">
      <span class="text-xs text-slate-500">
        화나요
        <i class="number block not-italic text-black">
          <span class="reaction-count"> <?php
            echo np_get_post_reaction_count($post_id, 'angry'); ?>
          </span>
        </i>
      </span>
      <div class="reaction-loading-indicator hidden"> <?php
        echo np_template_common('flash-loading-indicator'); ?>
      </div>
    </button>
  </div>