<div class="modal micromodal-slide" id="remove-reply-modal" aria-hidden="true">
  <div class="modal__overlay" tabindex="-1" data-micromodal-close>
    <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
      <header class="modal__header">
        <h2 class="modal__title">
          댓글을 삭제하시겠습니까?
        </h2>
        <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
      </header>
      <main class="modal__content">
        <p>비밀번호를 입력해 주세요</p>
        <input name="password" type="text" placehold="비밀번호" />
      </main>
      <footer class="modal__footer">
        <button class="modal__btn modal__btn-primary">삭제하기</button>
        <button class="modal__btn" data-micromodal-close aria-label="Close this dialog window">취소</button>
      </footer>
    </div>
  </div>
</div>