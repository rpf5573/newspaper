<?php
$current_category = get_queried_object(); ?>

<div class="desktop-category-section-1">
  <h2 class="category-title flex gap-1 items-end border-b-2 border-black pb-2 mb-4">
    <strong class="text-xl"><?php echo $current_category->name; ?></strong>
    <small class="text-sm text-slate-300"><?php echo $current_category->count; ?></small>
  </h3>
  <ul class="category-post-list flex flex-col gap-3"> <?php
    if (have_posts()) :
      while (have_posts()) : the_post();
        $category = get_the_category()[0];
        $category_link =  esc_url(get_category_link( $category->term_id)); ?>
        <li class="flex gap-2">
          <div class="left w-[25%]">
            <a href="<?php the_permalink(); ?>" class="flex gap-2">
              <img src="<?php echo np_get_image_url(get_post_thumbnail_id(), 'np-size-500x300'); ?>" alt="<?php echo np_get_image_alt_text(get_post_thumbnail_id()); ?>" />
            </a>
          </div>
          <div class="right flex-1 flex flex-col justify-between py-2">
            <a href="<?php the_permalink(); ?>" class="block">
              <h2 class="text-xl line-clamp-2 leading-[24px] max-h-[50px] mb-2"><?php echo get_the_title(); ?></h2>
              <p class="text-sm text-zinc-500 line-clamp-2 leading-[16px] max-h-[35px] mb-2">
                <?php echo get_the_excerpt(); ?>
              </p>
            </a>
            <div class="byline flex gap-2 text-zinc-500 text-sm items-center">
              <a href="<?php echo $category_link; ?>"><span><?php echo $category->name; ?></span></a>
              <div class="vertical-divider h-[12px] w-[1px] bg-zinc-500"></div>
              <a href="/"><span><?php the_author(); ?></span></a>
              <?php
                $date = get_the_date();
                if ($date) { ?>
                  <div class="vertical-divider h-[12px] w-[1px] bg-zinc-500"></div>
                  <span><?php echo $date; ?></span> <?php
                }
              ?>
            </div>
          </div>
        </li> <?php
      endwhile;
    endif;
  ?>
  </ul>
  <div class="pagination-container">
    <?php np_category_pagination(); // 안에서 echo를 한다 ?>
  </div>
</div>
