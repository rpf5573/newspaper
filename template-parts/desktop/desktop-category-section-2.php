<?php
  $desktop_category_section_2 = np_get_popular_posts(10, 1 * 24);
  if (count($desktop_category_section_2) < 10) {
    $desktop_category_section_2 = np_get_latest_posts(10, 1);
  }
?>

<div class="desktop-category-section-2 border-black"> <?php
  echo np_template_desktop('sidebar-head-1', [
    'html_title' => '<span class="text-red-500">오늘 핫한</span> 뉴스',
    'h1_class' => '!pb-3'
  ]); ?>
  <div class="swiper">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 0; $i < 5; $i += 1) {
            $post = $desktop_category_section_2[$i]; ?>
            <li class="mb-4 last:mb-0">
              <a href="<?php echo $post['post_link']; ?>" class="flex hover:underline">
                <p class="font-bold align-top w-8 <?php if ($i < 3) { echo "text-red-500"; } ?>"><?php echo $i + 1; ?></p>
                <div class="align-top flex-1">
                  <h2 class="text-base pr-2 line-clamp-3"><?php echo $post['title']; ?></h2>
                </div>
                <p class="w-[20%]">
                  <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-300x300'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>">
                </p>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 5; $i < 10; $i += 1) {
            $post = $desktop_category_section_2[$i]; ?>
            <li class="mb-4 last:mb-0">
              <a href="<?php echo $post['post_link']; ?>" class="flex hover:underline">
                <p class="font-bold align-top w-8 <?php if ($i < 3) { echo "text-red-500"; } ?>"><?php echo $i + 1; ?></p>
                <div class="align-top flex-1">
                  <h2 class="text-base pr-2 line-clamp-3"><?php echo $post['title']; ?></h2>
                </div>
                <p class="w-[20%]">
                  <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-300x300'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>">
                </p>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
    </div>
  </div>
</div>

