<div class="flex">
  <div class="left w-[75%] pr-3 mb-8"> <?php
    np_template_desktop('category-section-1'); ?>
  </div>
  <div class="right w-[25%] pl-3 relative">
    <div class="sticky top-2">
      <div class="mb-5">
        <?php np_template_desktop('category-section-2'); ?>
      </div>
      <div class="mb-5">
        <?php np_template_desktop('category-section-3'); ?>
      </div>
      <div class="mb-5">
        <?php np_template_desktop('category-section-4'); ?>
      </div>
    </div>
  </div>
</div>
