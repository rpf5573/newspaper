<?php
$desktop_front_section_1 = np_get_latest_posts(5);
if (count($desktop_front_section_1) < 5) {
  echo "글 개수가 부족합니다. 5개 이상 입력해 주세요";
  return;
} ?>

<div class="desktop-front-section-1 flex flex-row">
  <div class="basis-7/12">
    <div class="main-big-article h-full">
      <a href="<?php echo $desktop_front_section_1[0]['post_link']; ?>" class="block h-full relative img-hover-scale-1">
        <div class="relative overflow-hidden">
          <img src="<?php echo np_get_image_url($desktop_front_section_1[0]['thumbnail_image_id'], 'np-size-800x600'); ?>" alt="<?php echo np_get_image_alt_text($desktop_front_section_1[0]['thumbnail_image_id']); ?>">
        </div>
        <div class="entry-content absolute bottom-0 left-0 right-0 p-2 p-5 z-20">
          <h1 class="text-white line-clamp-3 font-normal text-3xl leading-[34px] max-h-[120px] lg:text-4xl"><?php echo $desktop_front_section_1[0]['title']; ?></h1>
        </div>
        <div class="title-cover h-[300px] z-10"></div>
      </a>
    </div>
  </div>
  <div class="basis-5/12 pl-4 flex flex-col justify-between"> <?php
    for ($i = 1; $i < 5; $i += 1) {
      np_template_desktop('article-row-2', array(
        'class' => '',
        'link_url' => $desktop_front_section_1[$i]['post_link'],
        'img_url' => np_get_image_url($desktop_front_section_1[$i]['thumbnail_image_id'], 'np-size-400x300'),
        'img_alt' => '',
        'title' => $desktop_front_section_1[$i]['title'],
        'date' => $desktop_front_section_1[$i]['date'],
      ));
    } ?>
  </div>
</div>
