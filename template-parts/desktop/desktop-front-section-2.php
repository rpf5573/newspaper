<?php
$desktop_front_section_2 = np_get_popular_posts(18, 3 * 24);
if (count($desktop_front_section_2) < 18) {
  $desktop_front_section_2 = np_get_latest_posts(18);
}
if (count($desktop_front_section_2) < 18) {
  echo "글 개수가 부족합니다. 18개 이상 입력해 주세요";
  return;
}
?>

<div class="desktop-front-section-2">
  <header class="border-b-2 border-b-black mb-2"> <?php
    np_template_desktop('sidebar-head-1', [
      'html_title' => '베스트 이슈',
      'h1_class' => '!pb-1 !mb-0',
    ]); ?>
  </header>
  <div class="swiper">
    <div class="swiper-wrapper mb-3">
      <div class="swiper-slide active">
        <div class="grid grid-cols-3 gap-4"> <?php
          for ($i = 0; $i < 9; $i += 1) { ?>
            <a href="<?php echo $desktop_front_section_2[$i]['post_link']; ?>">
              <div class="img-hover-scale-1 mb-1">
                <img src="<?php echo np_get_image_url($desktop_front_section_2[$i]['thumbnail_image_id'], 'np-size-600x400'); ?>" alt="<?php echo np_get_image_alt_text($desktop_front_section_2[$i]['thumbnail_image_id']); ?>" class="aspect-[3/2]">
              </div>
              <h2 class="line-clamp-2 text-sm leading-[20px] max-h-[42px] lg:text-base lg:leading-[22px] lg:max-h-[50px]">
                <?php echo $desktop_front_section_2[$i]['title']; ?>
              </h2>
            </a> <?php
          } ?>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="grid grid-cols-3 gap-4"> <?php
          for ($i = 9; $i < 18; $i += 1) { ?>
            <a href="<?php echo $desktop_front_section_2[$i]['post_link']; ?>">
              <div class="img-hover-scale-1 mb-1">
                <img src="<?php echo np_get_image_url($desktop_front_section_2[$i]['thumbnail_image_id'], 'np-size-600x400'); ?>" alt="<?php echo np_get_image_alt_text($desktop_front_section_2[$i]['thumbnail_image_id']); ?>" class="aspect-[3/2]">
              </div>
              <h2 class="line-clamp-2 text-sm leading-[20px] max-h-[42px] lg:text-base lg:leading-[22px] lg:max-h-[50px]">
                <?php echo $desktop_front_section_2[$i]['title']; ?>
              </h2>
            </a> <?php
          } ?>
        </div>
      </div>
    </div>
  </div>
</div>
