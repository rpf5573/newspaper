<?php
$post = $args['post']; ?>

<li>
  <a href="<?php echo $post['post_link']; ?>" class="flex flex-col mb-1">
    <div class="img-hover-scale-1 mb-1">
      <img src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-800x600'); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>" class="aspect-[2/1]">
    </div>
    <h2 class="line-clamp-2 text-sm leading-[20px] max-h-[42px] lg:text-base lg:leading-[24px] lg:max-h-[52px]">
      <?php echo $post['title']; ?>
    </h2>
  </a>
</li>
