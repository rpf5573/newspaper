<?php
$html_title = $args['html_title'] ?? '';
$h1_class = $args['h1_class'] ?? '';
?>

<h1 class="<?php echo $h1_class; ?> text-xl font-bold py-1 mb-1 flex items-center justify-between relative">
  <div class="left grow shrink basis-0"> <?php
    echo $html_title; ?>
  </div>
  <div class="swiper-pagination grow-0 shrink basis-0 !static text-end pr-2 !justify-end"></div>
</h1>