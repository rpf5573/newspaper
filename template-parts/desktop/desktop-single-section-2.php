<div class="desktop-single-section-2 article-single-content">
  <h2 class="text-2xl border-l-4 pl-4 border-slate-400">
    <?php the_title(); ?>
  </h2> <?php
  // Display the featured image, if available.
  if ( has_post_thumbnail() ) : ?>
    <div class="post-thumbnail my-5">
      <?php the_post_thumbnail( 'full', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
    </div>
  <?php endif; ?>
  <div class="content mb-2">
    <?php the_content(); ?>
  </div>
  
  <?php echo do_shortcode( '[korea_sns_pro_button]' ); ?>
  <p class="text-xs text-slate-500 text-right mt-8 mb-10"><?php echo np_get_copyright_message(); ?></p>
</div>
