<?php
$desktop_single_section_4 = np_get_popular_posts(10, 7 * 24);
if (count($desktop_single_section_4) < 10) {
  $desktop_single_section_4 = np_get_latest_posts(10);
} ?>

<div class="desktop-single-section-4"> <?php
  np_template_desktop('sidebar-head-1', [
    'html_title' => '가장 많이본 뉴스',
    'h1_class' => '!pb-3'
  ]); ?>
  <div class="swiper mb-2">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 0; $i < 5; $i += 1) {
            $post = $desktop_single_section_4[$i]; ?>
            <li class="mb-2">
              <a href="<?php echo $post['post_link']; ?>" class="flex items-center hover:underline">
                <div>
                  <img class="h-16 aspect-[4/3]" src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-400x300'); ?>" alt="">
                </div>
                <h2 class="ml-4"><?php echo $post['title'] ?></h2>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
      <div class="swiper-slide">
        <ul> <?php
          for ($i = 0; $i < 5; $i += 1) {
            $post = $desktop_single_section_4[$i]; ?>
            <li class="mb-2">
              <a href="<?php echo $post['post_link']; ?>" class="flex items-center hover:underline">
                <div>
                  <img class="h-16 aspect-[4/3]" src="<?php echo np_get_image_url($post['thumbnail_image_id'], 'np-size-400x300'); ?>" alt="">
                </div>
                <h2 class="ml-4"><?php echo $post['title'] ?></h2>
              </a>
            </li> <?php
          } ?>
        </ul>
      </div>
    </div>
  </div>
</div>
