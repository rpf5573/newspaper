<?php
$post_id = $args['post_id'];
$comment_id_list = np_get_comment_id_list($post_id); ?>

<div class="desktop-single-section-6">
  <div class="comment-area">
    <!-- comment write area -->
    <div class="my-10">
    <h2 class="text-xl font-bold"><?php echo count($comment_id_list); ?>개의 댓글</h2> <?php 
      np_template_common( 'comment-form', ['post_id' => $post_id] ); ?>
    </div> <?php
    if (count($comment_id_list) > 0) { ?>
      <ul class="[&>li:not(:first-child)]:border-t"> <?php
        foreach ($comment_id_list as $comment_id) {
          np_template_common( 'comment-list-item', ['comment_id' => $comment_id, 'post_id' => $post_id] );
        } ?>
      </ul> <?php
    } ?>
  </div>
</div>
