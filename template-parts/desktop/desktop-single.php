<div class="desktop-single">
  <script>
    if (window.innerWidth < 768) {
      var $self = document.querySelector('.desktop-single');
      $self.remove();       
    }
  </script>
  
  <?php
  // The WordPress Loop: let's retrieve our post.
  if (have_posts()):
    while ( have_posts() ) : the_post(); 
      $post_id = get_the_ID(); ?>
      <article id="post-<?php echo $post_id; ?>" <?php post_class(); ?>>
        <!-- 헤더 -->
        <div class="wrap"> <?php
          np_template_desktop('single-section-1'); ?>
        </div>

        <!-- 본문 -->
        <div class="wrap">
          <section class="flex">
            <div class="left w-[75%] pr-3 mb-8">
              <div class="pr-3">
                <div class="mb-7"> <?php
                  np_template_desktop('single-section-2'); ?>
                </div>
                <div class="mb-7 py-10 border-t border-b"> <?php
                  np_template_desktop('single-section-3'); ?>
                </div>
                <div class="mb-7"> <?php
                  np_template_desktop('single-section-4'); ?>
                </div>
                <div class="mb-7"> <?php
                  np_template_desktop('single-section-5'); ?>
                </div>
                <div class="mb-10"> <?php
                  np_template_desktop('single-section-6', ['post_id' => $post_id]); ?>
                </div>
              </div>
            </div>
            <div class="right w-[25%] pl-3 relative">
              <div class="sticky top-2">
                <!-- 실시간 인기뉴스 -->
                <div class="mb-5"> <?php
                np_template_desktop('single-section-7'); ?>
                </div>

                <!-- 최신뉴스 -->
                <div class="mb-5"> <?php
                  np_template_desktop('single-section-8'); ?>
                </div>

                <div class="pb-5"> <?php
                  np_template_desktop('single-section-9'); ?>
                </div>
              </div>
            </div>
          </section>
        </div>
      </article> <?php
    endwhile;
  endif;
  rewind_posts();
  ?>
</div>
