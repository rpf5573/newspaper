<?php

$category = get_the_category()[0];
$category_link =  esc_url(get_category_link( $category->term_id)); ?>
<li class="pb-4 border-b border-b-gray-100 mb-4">
  <a href="<?php the_permalink(); ?>" class="block mb-2 aspect-[5/3]">
    <img src="<?php echo np_get_image_url(get_post_thumbnail_id(), 'np-size-500x300'); ?>" alt="<?php echo np_get_image_alt_text(get_post_thumbnail_id()); ?>">
  </a>
  <a href="<?php the_permalink(); ?>" class="block">
    <h2 class="text-xl mb-2"><?php echo get_the_title(); ?></h2>
    <p class="text-sm text-zinc-400 line-clamp-2 mb-2">
      <?php echo get_the_excerpt(); ?>
    </p>
  </a>
  <div class="byline flex gap-2 text-zinc-300 text-xs items-center">
    <a href="<?php echo $category_link; ?>"><span><?php echo $category->name; ?></span></a>
    <div class="vertical-divider h-[12px] w-[1px] bg-zinc-500"></div>
    <a href="/"><span><?php the_author(); ?></span></a>
    <div class="vertical-divider h-[12px] w-[1px] bg-zinc-500"></div>
    <span><?php the_date(); ?></span>
  </div>
</li>
