<?php
$current_category = get_queried_object(); ?>

<main class="mobile-category-section-1">
  <h2 class="category-title flex gap-1 items-end border-b-2 border-black pb-2 mb-4">
    <strong class="text-xl"><?php echo $current_category->name; ?></strong>
    <small class="text-sm text-slate-300"><?php echo $current_category->count; ?></small>
  </h3>
  <ul class="mobile-category-article-list"> <?php
    if (have_posts()):
      while (have_posts()): the_post(); 
        np_template_mobile('category-section-1-list-item');
      endwhile;
    endif; ?>
  </ul>
  <button type="button" class="load-more mobile" data-current-page="1" data-category-id="<?php echo $current_category->term_id; ?>">
    <span>더보기</span>
  </button>
  <div class="no-more-posts mobile hidden">
    마지막 페이지입니다
  </div>
</main>
