<?php
$current_category = get_queried_object(); ?>

<div class="wrap mb-5"> <?php
  np_template_mobile('category-section-1');
?>
</div>

<div class="divider mb-5"></div>

<div class="wrap mb-5"> <?php
  np_template_mobile('category-section-2'); ?>
</div>

<div class="divider mb-5"></div>

<div class="wrap mb-3"> <?php
  np_template_mobile('category-section-3'); ?>
</div>
