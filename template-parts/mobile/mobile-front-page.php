<div class="mobile-front-page">
  <div class="wrap mb-5"> <?php
    np_template_mobile('front-section-1'); ?>
  </div>

  <div class="divider mb-5"></div>

  <div class="wrap"> <?php
    np_template_mobile('front-section-2'); ?>
  </div>

  <div class="divider mb-5"></div>

  <div class="wrap mb-5"> <?php
    np_template_mobile('front-section-3'); ?>
  </div>

  <div class="divider mb-5"></div>

  <div class="wrap mb-5"> <?php
    np_template_mobile('front-section-4'); ?>
  </div>
</div>
