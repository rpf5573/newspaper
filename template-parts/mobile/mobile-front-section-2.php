<?php
$mobile_front_section_2 = np_get_popular_posts(5, 1 * 24);
if (count($mobile_front_section_2) < 5) {
  $mobile_front_section_2 = np_get_latest_posts(5);
} ?>

<div class="mobile-front-section-2"> <?php
  np_template_mobile('head-1', [
    'html_title' => '오늘의 <span class="text-red-500">베스트</span>'
  ]); ?>
  <div class="swiper">
    <div class="swiper-wrapper"> <?php
      for ($i = 0; $i < count($mobile_front_section_2); $i += 1) {
        $post = $mobile_front_section_2[$i]; ?>
        <div class="swiper-slide">
          <a href="<?php echo $post['post_link']; ?>" class="flex flex-col relative mb-2">
            <img src="<?php echo np_get_image_url($post['thumbnail_image_id']); ?>" alt="<?php echo np_get_image_alt_text($post['thumbnail_image_id']); ?>" class="aspect-[8/5]">
            <h2 class="absolute bottom-3 left-0 right-0 z-20 px-2 font-normal text-base text-white line-clamp-2 leading-[24px] max-h-[52px]">
              <?php echo $post['title']; ?>
            </h3>
            <div class="title-cover h-[100px] z-10"></div>
          </a>
        </div>
      <?php } ?>
    </div>
    <div class="swiper-pagination relative"></div>
  </div>
</div>
