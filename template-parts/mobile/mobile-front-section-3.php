<?php
$mobile_front_section_3 = np_get_latest_posts(10); 
if (count($mobile_front_section_3) < 10) {
  echo "글을 10개 이상 작성해 주세요";
  return;
}
?>

<div class="mobile-front-section-3"> <?php
  np_template_mobile('head-1', [
    'html_title' => '<span class="text-red-500">최신</span> 뉴스',
    'pagination_count' => 2,
  ]); ?>
  <div class="mobile-front-section-3-slider">
    <div class="swiper">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          <div class="flex flex-col"> <?php
            for ($i = 0; $i < 5; $i += 1) {
              $post = $mobile_front_section_3[$i];
              np_template_mobile('article-row-1', array(
                'class' => '',
                'link_url' => $post['post_link'],
                'img_url' => np_get_image_url($post['thumbnail_image_id'], 'np-size-300x200'),
                'img_alt' => '',
                'aspect' => 'aspect-[3/2]',
                'title' => $post['title'],
              )); 
              if ($i !== 4) { ?>
                <div class="article-row-1-divider h-[1px] bg-gray-100 my-[12px]"></div> <?php
              }
            } ?>
          </div>
        </div>
        <div class="swiper-slide">
          <div class="flex flex-col"> <?php
            for ($i = 5; $i < 10; $i += 1) {
              $post = $mobile_front_section_3[$i];
              np_template_mobile('article-row-1', array(
                'class' => '',
                'link_url' => $post['post_link'],
                'img_url' => np_get_image_url($post['thumbnail_image_id'], 'np-size-300x200'),
                'img_alt' => '',
                'aspect' => 'aspect-[3/2]',
                'title' => $post['title'],
              )); 
              if ($i !== 9) { ?>
                <div class="article-row-1-divider h-[1px] bg-gray-100 my-[12px]"></div> <?php
              }
            } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
