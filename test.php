<nav itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement" id="main-nav" class="primary-nav">
  <div class="wrap clearfix has-search-box">
    <ul id="menu-menu-1" class="nav-menu clear">
      <li id="menu-item-1377"
        class="mega-menu mega-5col menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children menu-item-1377">
        <a href="#">Home</a>
      </li>
      <li id="menu-item-470"
        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-470"><a
          href="#">Destinations</a>
        <ul class="sub-menu" style="visibility: visible; display: none;">
          <li id="menu-item-471" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-471">
            <a href="#">Africa</a>
          </li>
          <li id="menu-item-472" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-472">
            <a href="#">Asia</a>
          </li>
          <li id="menu-item-473" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-473">
            <a href="#">Australia</a>
          </li>
          <li id="menu-item-474" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-474">
            <a href="#">Central America</a>
          </li>
          <li id="menu-item-475" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-475">
            <a href="#">Europe</a>
          </li>
          <li id="menu-item-476" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-476">
            <a href="#">India</a>
          </li>
          <li id="menu-item-477" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-477">
            <a href="#">Middle East</a>
          </li>
          <li id="menu-item-478" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-478">
            <a href="#">South America</a>
          </li>
        </ul>
      </li>
      <li id="menu-item-499"
        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-499"><a
          href="#">Blog</a>
        <ul class="sub-menu" style="visibility: visible; display: none;">
          <li id="menu-item-498" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-498"><a
              href="#">Grid Style 2 col</a></li>
          <li id="menu-item-497" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-497"><a
              href="#">Grid Style 3 col</a></li>
          <li id="menu-item-496" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-496"><a
              href="#">Grid Style 4 col</a></li>
          <li id="menu-item-495" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-495"><a
              href="#">
          <li id="menu-item-494" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-494"><a
              href="#">Full Post Style</a></li>
          <li id="menu-item-500" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-500"><a
              href="#">List Style</a></li>
        </ul>
      </li>
      <li id="menu-item-539"
        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-539"><a
          href="#">Pages</a>
        <ul class="sub-menu" style="visibility: visible; display: none;">
          <li id="menu-item-537" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-537"><a
              href="#">Typography</a>
          </li>
          <li id="menu-item-536" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-536"><a
              href="#">Components</a>
          </li>
          <li id="menu-item-538" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-538"><a
              href="#">Left Sidebar</a></li>
        </ul>
      </li>
      <li id="menu-item-627" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-627"><a
          href="#">Shop</a></li>
      <li id="menu-item-525" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-525"><a
          href="#">About</a></li>
      <li id="menu-item-526" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-526"><a
          href="#">Contact</a></li>
      <li id="menu-item-706" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-706"><a
          href="#">Food Demo</a></li>
    </ul>
    <div class="inline-search-box"><a class="search-trigger" href="#"><span class="screen-reader-text">Open search
          panel</span></a>
      <form role="search" method="get" class="search-form"
        action="https://labs.saurabh-sharma.net/themes/newsplus/demo-3/">
        <label>
          <input type="search" class="search-field" placeholder="Search …" value="" name="s">
        </label>
        <input type="submit" class="search-submit" value="Search">
      </form>
    </div><!-- /.inline-search-box -->
  </div><!-- .primary-nav .wrap -->
</nav><!-- #site-navigation -->
